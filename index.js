const path = require('path');
const fs = require('fs');
const express = require('express');
const multer = require('multer');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const Video = require('./Video');

const app = express();

dotenv.config();

mongoose.connect(`mongodb+srv://crowddigital21:crowddigital21@cluster0.l8e0f.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`).then(() => {
    console.log('Connected');
}).catch((err) => {
    console.log('Error: ', err);
})

const dirname = path.resolve();
app.use(express.json());
app.use('/uploads', express.static(path.join(dirname, '/uploads')));

const storage = multer.diskStorage({
  destination(req, file, cb) {
    // console.log('dest: ', file);
    cb(null, 'uploads/')
  },
  filename(req, file, cb) {
    // console.log('filename: ',file);
      const filename = `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`;
    cb(
      null,
      filename
      )
    },
  })
  
  function checkFileType(file, cb) {
    console.log('check', file);
    // const filetypes = /jpg|jpeg|png|mp4|webm|mpeg/;
    // const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // const mimetype = filetypes.test(file.mimetype);
    
    if ( file.mimetype.includes('video')) {
      return cb(null, true)
    } else {
      cb('Video only!')
    }
  }
  
  const upload = multer({
    storage,
    fileFilter: function (req, file, cb) {
      checkFileType(file, cb)
    },
  },);

app.get('/', async(req, res) => {
    res.send('Welcome to Video')
})

app.post('/videos', upload.single('file'), async (req, res, next) => {
    console.log('video');
    console.log(req.file);
    try {
        const video = new Video({url: `/uploads/${req.file.filename}`});
        await video.save();
        console.log(video);
        res.json(video);
    } catch (error) {
        console.log(error);
        res.json({error});
        
    }
    
})

app.get('/videos', async(req, res) => {
    try {
        const videos = await Video.find();
        console.log(videos);
        res.json(videos);
    } catch (error) {
        console.log(error);
        res.json({error: 'An Error Occured.'})
    }
})

app.get('/videos/:id', async(req, res) => {
    try {
        const video = await Video.find({_id: req.params.id});
        console.log(video);
        res.json(video);
    } catch (error) {
        console.log(error);
        res.json({error: 'An Error Occured.'})
    }
})

app.delete('/videos/:id', async(req, res) => {
  console.log('get videos');
  try {
      await Video.findByIdAndDelete(req.params.id);
      res.json({message: 'Video is deleted.'});
  } catch (error) {
      console.log(error);
      res.json({error: 'An Error Occured.'})
  }
})

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log('Server is running.');
})
